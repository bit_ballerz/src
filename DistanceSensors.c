#include "simpletools.h"                       // Include simpletools header
#include "ping.h"                              // Include ping header
int detect;                                    // initialize global variable for detection
int points = 0;                                // initial points

int main()                                     // Main function
 {
  while(1)
 {
   int feetDistance = ping_cm(15);                //ping ultrasonic to pin 15, used for tracking body
   int rimDetection = ping_cm(16);                //laser sensor to pin 14, used for ball detection
  high(26);                                    //used to verify EEPROM has been loaded properly
  pause(100);       
  low(26);                                     //used to verify EEPROM has been loaded properly
  print("points = %d\n",points); 
  pause(1000);
  if (rimDetection < 6)          
 {
     detect = 1;                       
     // print("Object Detected\n")
     if(feetDistance < 40)
      { 
       points = points + 2;                     //2 points if within 40cm of ping ultrasonic sensor
       print("points = %d\n",points);           //data to be sent serially to rpi
       pause(5000);
      } 
     else
      {  
       points = points + 3;                     //3 points if beyond 40cm of ping ultrasonic sensor 
       print("points = %d\n",points);           //data to be sent serially to rpi
       pause(5000);
      }
  }
   else
   {
    detect = 0;                                  //reset 
    print("points = %d\n",points); 
   }                
 }
}